<?php
//Add an additional form to receive information form
/*
##################################################################
Written by "Shahin Ataei" for "Meneldor project" ===> '06-April-2016'
##################################################################
shahin.ataei.1990@gmail.com
https://www.linkedin.com/in/shahin-ataei-547983121
https://meneldor.co
*/
class FormPlugin_AddAnother extends FormPlugin{
    
    //List of variable
    public  $public_Section;
    public  $FormSettings;
    public  $Form_Counter;
    
    private $self_FormInfo;
    private $Messages = [
        'This form is not exist please recheck your form id then check this path => FormPlugin_AddAnother->formExist()',
        'You don\'t have access to this form please recheck form id then check this path => FormPlugin_AddAnother->access()'
    ];
    
    public function __construct($Params) {
       //Fetch and set parent property value
       foreach($Params as $Key => $Param){
	       $this->{$Key} = $Param;
       }
    }
    
    
    //Check if called form is exist
    private function formExist(){
        
        $Where = array(
            'Id = :Id: and Deleted = 0',
            'bind' => array(
                'Id' => $this->Form_Id
            )
        );
        $this->Models->Find('Forms',$Where);
        $Info = $this->Models->Owner;
        if($Info){
            $this->self_FormInfo = $Info;
            return true;
        }
        $this->Error->handle(
            [
                'result' => 1,
                'message' => $this->Messages[0]
            ]
        );
    }
    
    //Return path access of current form
    private function access(){
		if($this->self_FormInfo->Section == $this->public_Section){
            return true;
        }
        $this->Error->handle(
            [
                'result' => 2,
                'message' => $this->Messages[1]
            ]
        );
    }
    
    //Return prepared and calculated form information
    private function prepareForm(){
        
        $info = $this->self_FormInfo;
        
        $formElements = json_decode($info->Elements,true);
        
        $formElements = [
            'Id' => $info->Id,
            'Elements' => $formElements,
            'ExTable' => $info->ExTable,
            'TableStructer' => $info->TableStructer,
            'Repetition' => $info->Repetition,
            'Base' => $info->Base,
            'Relation' => $info->Relation,
            'Title' => $info->Title,
            'Alias' => $info->Alias
        ];
        $this->FormSettings = $formElements;
    }
    
    
    //Prepared session cunter
    private function counter(){
        $Counter = $this->session->get('Form_'.$this->Form_Id);
        $Counter = $Counter+1;
        $this->Form_Counter = $Counter;
        $this->session->set('Form_'.$this->Form_Id , $Counter);
    }
    
    
    
    //Run to add another form skeleton to application
	public function add($Form = '0'){
        $this->Form_Id = $Form;
        $this->formExist();
        $this->access();
        $this->prepareForm();
        $this->counter();
    }
}
?>