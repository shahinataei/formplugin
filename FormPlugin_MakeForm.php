<?php
/*
##################################################################
Written by "Shahin Ataei" for "Meneldor project" ===> '06-April-2016'
##################################################################
shahin.ataei.1990@gmail.com
https://www.linkedin.com/in/shahin-ataei-547983121
https://meneldor.co
*/
class FormPlugin_MakeForm extends FormPlugin{
    
    
    private $Messages = [
        'This section does not exist, please re-check the "section Id" to ensure the correct "section Id" and then check this path: FormPlugin=>FormPlugin_AddAnother=>fetchInfo()'
    ];
    
    
    public function __construct($Params) {
       //Fetch and set parent property value
       foreach($Params as $Key => $Param){
	       $this->{$Key} = $Param;
       }
    }
    
    
	//Fetch forms settings from database
	//And Set to FormSettings property
	private function fetchInfo(){//return "Bool" value and set some value
		$FormSection = $this->FormSection;
		$Query = '
			Select 
			FormSections.Id as FormSectionsId,FormSections.Title as FormSectionsTitle,FormSections.Alias as FormSectionsAlias,FormSections.DisplayPath,FormSections.SendPath,FormSections.CheckPath,FormSections.AddAnother,
			FormSteps.Id as FormStepsId,FormSteps.Sections,FormSteps.Title as FormStepsTitle,FormSteps.Alias as FormStepsAlias,
			Forms.Id as FormId,Forms.Elements,Forms.ExTable,Forms.TableStructer,Forms.Repetition,Forms.Base,Forms.Relation,Forms.Alias as FormAlias,Forms.Title as FormTitle
			From
			FormSections,
			FormSteps,
			Forms
			WHERE 
			FormSections.Id = '.$FormSection.' and FormSections.Deleted = 0
			and FormSteps.Sections = FormSections.Id and FormSteps.Deleted = 0
            and Forms.Step = FormSteps.Id and Forms.Deleted = 0
		';
        $this->Models->Phql($Query);
        $Info = $this->Models->Owner;
        if($Info){
           $this->FormSettings = $this->Models->Owner;//Set form database to varibale and
           return true;//Then return true value
        }
        $this->Error->handle(
            [
                'result' => 1,
                'message' => $this->Messages[0]
            ]
        );
	}
    
    
    //Make array from element
    private function updateArray($UpdateInfo,$formElements,$Element){
        $finalFormElements = $formElements;
        if(!empty($UpdateInfo[0])){
            foreach($UpdateInfo as $Key => $upInfo){
                foreach($formElements as $Key => $FElement){
                    $NewValue = $FElement['Name'];
                    $finalFormElements[$Key]['Value'] = $upInfo[$NewValue];
                }
                $Array[] = [
                    'Id' => $Element['FormId'],
                    'Elements' => $finalFormElements,
                    'ExTable' => $Element['ExTable'],
                    'TableStructer' => $Element['TableStructer'],
                    'Repetition' => $Element['Repetition'],
                    'Base' => $Element['Base'],
                    'Relation' => $Element['Relation'],
                    'Title' => $Element['FormTitle'],
                    'Alias' => $Element['FormAlias']
                ];
            }
        }else{
             foreach($formElements as $Key => $FElement){
                 $NewValue = $FElement['Name'];
                 $finalFormElements[$Key]['Value'] = $UpdateInfo[$NewValue];
             }
             $Array[] = [
                 'Id' => $Element['FormId'],
                 'Elements' => $finalFormElements,
                 'ExTable' => $Element['ExTable'],
                 'TableStructer' => $Element['TableStructer'],
                 'Repetition' => $Element['Repetition'],
                 'Base' => $Element['Base'],
                 'Relation' => $Element['Relation'],
                 'Title' => $Element['FormTitle'],
                 'Alias' => $Element['FormAlias']
             ];
        }
        return $Array;
    }
    
    
    //fetch table information for dipslay on update form
    private function findUpdate($Type,$Element,$baseId,$formElement){
            if($Type === 'Update' and $Element['Base'] === '1'){
                $Where = [
                    'Id = :Id: and Deleted = 0',
                    'bind' => [
                        'Id' => $baseId
                    ]
                ];
                $this->Models->Find($Element['ExTable'],$Where);
                $Array = get_object_vars($this->Models->Owner);
            }elseif($Type === 'Update' and $Element['Base'] === '0'){
                $Where = [
                    'Relation = :Relation: and Deleted = 0',
                    'bind' => [
                        'Relation' => $baseId
                    ]
                ];
                $this->Models->FindAll($Element['ExTable'],$Where);
                foreach($this->Models->Owner as $Val){
                    $Array[] = get_object_vars($Val);
                }
            }
            if(!empty($Array)){
                $result = $this->updateArray($Array,$formElement,$Element);
                return $result;
            }
                return false;
    }
    
    
    //Preparation of fetched information from the database, to send to view
    private function prepareInfo($Type = 'Display'){
        $Info = $this->FormSettings;
        
        $result = [];
        foreach($Info as $Element){
            
            $result['Section'][$Element['FormSectionsId']] = [
                'Id' => $Element['FormSectionsId'],
                'Title' => $Element['FormSectionsTitle'],
                'Alias' => $Element['FormSectionsAlias'],
                'DisplayPath' => $Element['DisplayPath'],
                'SendPath' => $Element['SendPath'],
                'AddAnother' => $Element['AddAnother'],
                'CheckPath' => $Element['CheckPath']
            ];
            
            $result['Steps'][$Element['FormStepsId']] = [
                'Id' => $Element['FormStepsId'],
                'Title' => $Element['FormStepsTitle'],
                'Alias' => $Element['FormStepsAlias']
            ];
            
            
            $formElements = json_decode($Element['Elements'],true);
            if($Type === 'Update'){
                $formElements = $this->findUpdate($Type,$Element,$this->baseId,$formElements); 
                $result['Forms'][$Element['FormStepsId']][$Element['FormId']] = $formElements;
            }else{
                $result['Forms'][$Element['FormStepsId']][$Element['FormId']] = [
                    'Id' => $Element['FormId'],
                    'Elements' => $formElements,
                    'ExTable' => $Element['ExTable'],
                    'TableStructer' => $Element['TableStructer'],
                    'Repetition' => $Element['Repetition'],
                    'Base' => $Element['Base'],
                    'Relation' => $Element['Relation'],
                    'Title' => $Element['FormTitle'],
                    'Alias' => $Element['FormAlias']
                ];
            }
        }
        
        $this->FormSettings = $result;
    }

    
    
    //Prepare information to display when update 
    private function prepareUpdateInfo(){
        
    }
    
    
    //Make new form elements
    public function newForm($Section){
        $this->FormSection = $Section;
        $this->fetchInfo();
        return $this->prepareInfo();
    }
    
    
    //Display update from
    public function updateForm($Section,$BaseId){
        $this->FormSection = $Section;
        $this->baseId = $BaseId;
        $this->fetchInfo();
        $this->prepareInfo('Update');
    }
    
}
?>