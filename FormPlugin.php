<?php
use Phalcon\Mvc\User\Plugin;

/*
A pluggin for making,check and send form section in application
##################################################################
Written by "Shahin Ataei" for "Meneldor project" ===> '06-April-2016'
##################################################################
shahin.ataei.1990@gmail.com
https://www.linkedin.com/in/shahin-ataei-547983121
https://meneldor.co
*/
class FormPlugin extends BasePlugin{
	
    protected $Params;
    protected $Parametr;
    //Settings
	protected $FormSection;
    
    private $selfMessages = [
        'This value is not exist on database',
        'This value is alredy exist on database'
    ];
    
    
    public $public_SectionId;
    
    public function __construct($Params = [true]){
        $this->Params = $Params;
        parent::maker($Params);
    }
    
    //Call sub class to make and manage our form
	public function caller($Name){
        $Class = 'FormPlugin_' . $Name;
        //Set class property to new child class
		return new $Class($this->Params);
	}
    
    
    public function checkValueData($Structure,$Table,$Column,$Value){
		if($Structure == 'Json'){
			$Value = '%"'.$Column.'":"'.$Value.'"%';
			$Where = array(
				"conditions" => "Info LIKE :Value:",
				"bind" => array(
					"Value" => $Value
				)
			);
			$this->Models->Find($Table,$Where);
			if($find){
				return true;
			}
            return false;
		}
		if($Structure == 'Separate'){
			$Where = array(
				"$Column = :Value:",
				"bind" => array(
					"Value" => $Value
				)
			);
			$this->Models->Find($Table,$Where);
			if($this->Models->Owner){
				return true;
			}
            return false;
		}
    }
    
    
	//Calculate and checking value sent by user and display result as json
	public function CheckValue($Name,$Value,$FormId){
        $Section = $this->public_SectionId;
		$this->Models->Find('Forms',"Id = $FormId and Section = $Section");
		$Column = $Name;
		$Value = $Value;
		$Result = $this->checkValueData($this->Models->Owner->TableStructer,$this->Models->Owner->ExTable,$Column,$Value);
		if($Result){
            $this->Error->handle(
                [
                    'result' => 0,
                    'message' => ''
                ]
            );
		}else{
            $this->Error->handle(
                [
                    'result' => 1,
                    'message' => ''
                ]
            );
		}
	}
}
?>