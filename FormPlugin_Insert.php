<?php
/*
##################################################################
Written by "Shahin Ataei" for "Meneldor project" ===> '06-April-2016'
##################################################################
shahin.ataei.1990@gmail.com
https://www.linkedin.com/in/shahin-ataei-547983121
https://meneldor.co
*/
class FormPlugin_Insert extends FormPlugin{
    
    
    private $Messages = [
        'This form is not exist or access',
        'This regex is not valie',
        'This field is required',
        'This value is not exist on database',
        'This value is alredy exist on database',
        'This value is not exist on forms data',
        'Your insert data was sucessfuly',
        'There was a problem when inserting information into the database.This can happen for the following reasons:<br>1-The table does not exist.<br>2-All input is in trouble'
    ];
    private $Data;
    
    public $public_Section;
    public $public_formId;
    public $public_fetchData;
    public $public_FinalValue = [];
    
    public function __construct($Params) {
       //Fetch and set parent property value
       foreach($Params as $Key => $Param){
	       $this->{$Key} = $Param;
       }
    }
    
    
	//Fetch forms settings from database
	//And Set to FormSettings property
	private function fetchInfo(){//return "Bool" value and set some value
		$Where = [
            'Id = :Id: and Deleted = 0 and Section = :Section:',
            'bind' => [
                'Id' => $this->public_formId,
                'Section' => $this->public_Section
            ]
        ];
        $this->Models->Find('Forms',$Where);
        $Info = $this->Models->Owner;
        if($Info){
            $Elements = json_decode($Info->Elements, true);
            $Data = [
                'Step' => $Info->Step,
                'Section' => $Info->Section,
                'Title' => $Info->Title,
                'Alias' => $Info->Alias,
                'Elements' => $Elements,
                'ExTable' => $Info->ExTable,
                'TableStructer' => $Info->TableStructer,
                'Repetition' => $Info->Repetition,
                'Base' => $Info->Base,
                'Relation' => $Info->Relation
            ];
            $this->Data = $Data;
        }else{
            $this->Error->handle(
                [
                    'result' => 1,
                    'message' => $this->Messages[0]
                ]
            );
        }
	}
    
	
    //Check string value by regex
    private function validation($Type,$Regex,$Value){
        
		$this->Validation->{$Type} = $Regex;
		
		if($this->Validation->eCheck($Type,$Value)){
			return true;
		}
        $this->Error->handle(
            [
                'result' => 2,
                'message' => $this->Messages[1] . ' => Element is ' . $Type
            ]
        );
        
    }
    
    
	//Cehck if value is exist
	private function CheckExist($Structure,$Table,$Column,$Value){
        
        if(!$this->checkValueData($Structure,$Table,$Column,$Value)){
            $this->Error->handle(
                [
                    'result' => 1,
                    'message' => $this->Messages[3] . ' => Element is ' . $Column
                ]
            );
        }
        
	}
    
    
    //Cehck if value is exist
    private function checkNotExist($Structure,$Table,$Column,$Value){
        if($this->checkValueData($Structure,$Table,$Column,$Value)){
            $this->Error->handle(
                [
                    'result' => 2,
                    'message' => $this->Messages[4] . ' => Element is ' . $Column
                ]
            );
        }
    }
    
    
    //Check Data Id value
    private function checkData($DataId,$Index,$Element){
        $this->Models->IndexData($DataId,$Index);
        if(!$this->Models->Owner){
                $this->Error->handle(
                    [
                        'result' => 4,
                        'message' => $this->Messages[5] . ' => Element is ' . $Element
                    ]
                );
        }
    }
    
    
    //Check Mutliple Data
    private function checkDataMultiple($DataId,$Index,$Element){
        $Exploder = explode(',',$Index);
        foreach($Exploder as $_Index){
            $this->Models->IndexData($DataId,$_Index);
            if(!$this->Models->Owner){
                    $this->Error->handle(
                        [
                            'result' => 4,
                            'message' => $this->Messages[5] . ' => Element is ' . $Element
                        ]
                    );
            }
        }
    }
    
    
    //Check Data table value
    private function checkDataTable($DataTable,$Columns,$Index,$Element){
        $Where = [
            $Columns['Index'] . ' = :Index: and Deleted = 0',
            'bind' => [
                'Index' => $Index
            ]
        ];
        $this->Models->Find($DataTable,$Where);
        if(!$this->Models->Owner){
                $this->Error->handle(
                    [
                        'result' => 4,
                        'message' => $this->Messages[5] . ' => Element is ' . $Element
                    ]
                );
        }
    }    
    
    
    //Check Data table value
    private function checkDataTableMutliple($DataTable,$Columns,$Index,$Element){
        $Exploder = explode(',',$Index);
        foreach($Exploder as $_Index){
            $Where = [
                $Columns['Index'] . ' = :Index: and Deleted = 0',
                'bind' => [
                    'Index' => $Index
                ]
            ];
            $this->Models->Find($DataTable,$Where);
            if(!$this->Models->Owner){
                    $this->Error->handle(
                        [
                            'result' => 4,
                            'message' => $this->Messages[5] . ' => Element is ' . $Element
                        ]
                    );
            }
        }
    }
    
    
    
    //Calculate sent data by user
    private function calculate(){
        $Elements = $this->Data;
        $FormElements = $Elements['Elements'];
        $Data = $this->public_fetchData;
        $Json = [];
        
        foreach($FormElements as $Element){
            
            //Check required
            if(isset($Element['require']) and $Element['require'] == 'true' and (!isset($Data[$Element['Name']]) or empty($Data[$Element['Name']]))){
                $this->Error->handle(
                    [
                        'result' => 3,
                        'message' => $this->Messages[2] . ' => Element is ' . $Element['Name']
                    ]
                );
            }
            
            
            //Check regex
            if(isset($Element['regex']) and !empty($Element['regex']) and (isset($Data[$Element['Name']]) and !empty($Data[$Element['Name']]))){
                $this->validation($Element['Name'],$Element['regex'],$Data[$Element['Name']]);
            }
            
            
            //Check exist
            if(isset($Element['check-exist']) and $Element['check-exist'] == 'true' and (isset($Data[$Element['Name']]) and !empty($Data[$Element['Name']]))){
                if(isset($Element['json']) and $Element['json'] == 'true'){$Structure = 'Json';}else{$Structure = 'Separate';}
                $this->CheckExist($Structure,$Elements['ExTable'],$Element['Name'],$Data[$Element['Name']]);
            }
            
            
            //Check not exist
            if(isset($Element['check-notexist']) and $Element['check-notexist'] == 'true' and (isset($Data[$Element['Name']]) and !empty($Data[$Element['Name']]))){
                if(isset($Element['json']) and $Element['json'] == 'true'){$Structure = 'Json';}else{$Structure = 'Separate';}
                $this->checkNotExist($Structure,$Elements['ExTable'],$Element['Name'],$Data[$Element['Name']]);
            }
            
            //Check data id value
            if(($Element['Type'] === 'SelectBox' or $Element['Type'] === 'Radio' or $Element['Type'] === 'Multiple') and isset($Element['DataId']) and $Element['DataId'] >= '1' and (isset($Data[$Element['Name']]) and !empty($Data[$Element['Name']]))){
                if($Element['Type'] === 'Multiple'){
                    $this->checkData($Element['DataId'],$Data[$Element['Name']],$Element['Name']);
                }
                $this->checkDataMultiple($Element['DataId'],$Data[$Element['Name']],$Element['Name']);
            }
            
            //Check data table value
            if(($Element['Type'] === 'SelectBox' or $Element['Type'] === 'Radio' or $Element['Type'] === 'Multiple') and isset($Element['DataTable']) and !empty($Element['DataTable']) and (isset($Data[$Element['Name']]) and !empty($Data[$Element['Name']]))){
                if($Element['Type'] === 'Multiple'){
                    $this->checkDataTableMutliple($Element['DataTable'],$Element['DataColumn'],$Data[$Element['Name']],$Element['Name']);
                }
                $this->checkDataTable($Element['DataTable'],$Element['DataColumn'],$Data[$Element['Name']],$Element['Name']);
            }
            
            
            //Convert password to encrypt
            if($Element['Type'] === 'Password'){
                $Data[$Element['Name']] = $this->Security->Encrypt($Data[$Element['Name']]);
            }
            
            //Calculate final value to insert on database
            if(isset($Element['DataTable']) and $Element['json'] === 'true'){
                $Json[$Element['Name']] = $Data[$Element['Name']];
            }else{
                $this->public_FinalValue[$Element['Name']] = $Data[$Element['Name']];
            }
        }
        
        $Json = json_encode($Json);
        $this->public_FinalValue['Info'] = $Json;
        $this->ExTable = $Elements['ExTable'];
        $this->TableStructer = $Elements['TableStructer'];
        $this->Base = $Elements['Base'];
    }
    
    //Insert value to base table
    private function insertBase(){
        $RandomId = rand(100,1000);
        $this->Models->Add($this->ExTable,$this->public_FinalValue);
        if($this->Models->Owner){
            $Id = $this->Models->Owner->Id;
            $this->session->set('InsertId'.$RandomId,$Id);
            //print_r($_SESSION);
            $this->Error->handle(
                [
                    'result' => 0,
                    'message' => $this->Messages[6],
                    'InsertId' => $RandomId
                ]
            );
        }else{
            $this->Error->handle(
                [
                    'result' => 6,
                    'message' => $this->Messages[7]
                ]
            );
        }
    }
    
    
    
    //Insert value to relation table
    private function insertRelation(){
       $Relation = $this->session->get('InsertId'.$this->InsertId);
       $this->public_FinalValue['Relation'] = $Relation;
       $this->Models->Add($this->ExTable,$this->public_FinalValue);
       if($this->Models->Owner->Id > 0){
            $this->Error->handle(
                [
                    'result' => 0,
                    'message' => $this->Messages[6]
                ]
            );
       }else{
            $this->Error->handle(
                [
                    'result' => 6,
                    'message' => $this->Messages[7]
                ]
            );
        }
    }
    

    
    //Insert final value to database
    private function insert(){
        if($this->Base === '1'){
            $this->insertBase();
        }else{
            $this->insertRelation();
        }
    }
    
    
    //Run function to insert data
    public function run($Type = 'Insert'){
        $this->fetchInfo();
        $this->calculate();
        if($Type === 'Insert'){
            $this->insert();
        }else{
            $this->update();
        }
    }
}
?>